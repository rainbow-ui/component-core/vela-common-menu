'use strict';

module.exports = {
    UnicornPermissionMenu: require('./component/permissionMenu/PermissionMenu'),
    Filter: require('./component/filter/Filter')
};