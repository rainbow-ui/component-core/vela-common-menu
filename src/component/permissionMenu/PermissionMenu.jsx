import { UIMenuBar, UICell, UIMenu, UIMenuItem, UISubMenu, Param, UISeparator, Component, I18nUtil } from "rainbowui-desktop-core";
import { UrlUtil } from "rainbow-desktop-tools";
import { SessionContext, StoreContext } from "rainbow-desktop-cache"
import defaultLogo from "../../image/logo.png";

export default class Menu extends Component {

	constructor(props) {
		super(props);
		this.state = {
			gitBookId: this.props.gitBookId ? this.props.gitBookId : 'gitbook_doccenter',
			defaultIcon: "glyphicon glyphicon-tag",
			permessionMenuList: [],
			user: {},
			defaultTenant: 'default',
			langs: [],
			tenants: [],
			logout: 'Logout',
			modelType: this.props.modelType,
			moduleId: this.props.moduleId,
			moduleGroupId: this.props.moduleGroupId,
			getSettings: { block: null, method: 'GET', dataType: 'json', contentType: 'application/json; charset=UTF-8' },
			postSettings: { block: null, method: 'POST', dataType: 'json', contentType: 'application/json; charset=UTF-8' }
		}
	}

	componentWillMount() {
		this.initUserInfo();
		if (JSON.parse(sessionStorage.getItem("project_config")).UI_SELECTTENANTS) {
			this.initTenantInfo();
		}
	}

	renderComponent() {
		return (
			<UIMenuBar id="menuBar" logo={this.renderLogo.bind(this)} backgroundWeight="inverse" styleClass="primary" fixtop="true"
				headerTitle={i18n[this.props.headerTitle]} subHeaderTitle={this.props.subHeaderTitle}
				headerTitleTarget={this.props.headerTitleTarget} onSubHeaderTitleClick={this.props.onSubHeaderTitleClick}>
				<UICell type="flex" className="nav-menu-bar" >
					<UICell type="flex">
						<UIMenu>
							{this.renderPermessionMenu()}
						</UIMenu>
					</UICell> 
					{/* <UICell type="flex" className="navMenu">
						<UIMenu>
							<UISubMenu icon="rainbow Help" value="UserManual">
								<UIMenuItem value="UserManual" onClick={this.onUserManualClick.bind(this)} />
							</UISubMenu>
						</UIMenu>
					</UICell> */}
					{/* {this.getTenantsSelect()} */}
					<UICell type="flex" >
						<UIMenu style={{ justifyContent: "flex-end" }}>
							<UISubMenu noI18n='true' value={this.state.user.RealName} icon="glyphicon glyphicon-user" className="dropdown-menu-right">
								{this.state.langs}
								<UISeparator />
								{this.state.tenants}
								{!_.isEmpty(this.state.tenants) ? <UISeparator /> : <noscript />}
								<UIMenuItem value="LogOut" onClick={this.onClickLogout} />
							</UISubMenu>
						</UIMenu>
					</UICell>
				</UICell>
			</UIMenuBar>
		);
	}

	onUserManualClick() {
		if (gitBookHelper) {
			var item = gitBookHelper.find((item) => item.id == this.state.gitBookId);
			if (item) {
				let bookName = item.name;
				let lang = localStorage.getItem('system_i18nKey');
				let url = item.gitBook.rootUrl + item.gitBook.bookUrl;
				window.open(url);
			}
		}

	}

	initUserInfo() {
		let get_lang_url = UrlUtil.getConfigUrl("UI_API_GATEWAY_PROXY", "COMMON", "GET_LANG");
		AjaxUtil.call(get_lang_url).then((langs) => {
			_.each(langs, (lang) => {
				this.state.langs.push(
					<UIMenuItem value={lang.LanguageId} onClick={this.selectLanguage.bind(this, lang.LanguageId)}>
						<Param name="Language" value={lang.LanguageId} />
					</UIMenuItem>
				);
			})

			let path_key = window.location.href;
			path_key = path_key.substring(0, path_key.indexOf("#"));

			if (SessionContext.get(path_key)) {
				this.setState({ permessionMenuList: SessionContext.get(path_key) });
			} else {
				let userUrl = UrlUtil.getConfigUrl("UI_API_GATEWAY_PROXY", "USER", "USER_INFO");
				AjaxUtil.call(userUrl).then((user) => {
					this.state.user = user;
					let load_menu_url = UrlUtil.getConfigUrl("UI_API_GATEWAY_PROXY", "PERMISSION", "LOAD_MENU");
					let params = { modelType: this.state.modelType, moduleId: this.state.moduleId, moduleGroupId: this.state.moduleGroupId, userId: user.UserId };
					for (let key in params) {
						if (params[key] == undefined) {
							delete params[key];
						}
					}
					AjaxUtil.call(load_menu_url, params, this.state.getSettings).then(
						(menus) => {
							this.setState({ permessionMenuList: menus });
							SessionContext.put(path_key, menus);
						},
						(err) => {
							this.setState({ permessionMenuList: null });
						}
					);
				});
			}
		});
	}

	getTenantsSelect() {
		return !_.isEmpty(this.state.tenants) ? <UICell type="flex" >
			<UIMenu >
				<UISubMenu noI18n='true' icon="rainbow Team" value={this.state.defaultTenant}>
					{this.state.tenants}
				</UISubMenu>
			</UIMenu>
		</UICell> : null
	}

	async initTenantInfo() {
		let get_env_url = UrlUtil.getConfigUrl("UI_API_GATEWAY_PROXY", "COMMON", "GET_TENANTS");
		await AjaxUtil.call(get_env_url).then((tenants) => {
			_.each(tenants, (tenant) => {
				this.state.tenants.push(
					<UIMenuItem value={tenant.TenantCode} noI18n='true' onClick={this.selectTenant.bind(this, tenant.TenantCode)}>
						<Param name="Tenant" value={tenant.TenantCode} />
					</UIMenuItem>
				);
			})

		});
		sessionStorage.setItem("x-ebao-tenant-code", sessionStorage.getItem("x-ebao-tenant-code") ? sessionStorage.getItem("x-ebao-tenant-code") : JSON.parse(sessionStorage.getItem("project_config")).UI_TENANT ? JSON.parse(sessionStorage.getItem("project_config")).UI_TENANT : 'ebaogi')
		this.setState({ tenants: this.state.tenants, defaultTenant: sessionStorage.getItem("x-ebao-tenant-code") })
	}

	renderLogo() {
		let logo = this.props.logo ? this.props.logo : defaultLogo;
		return <div className="logoimg"><img src={logo} />{i18n[this.props.label]}</div>;
	}

	renderPermessionMenu() {
		let _self = this;
		let permessionMenu = [];
		let permessionMenuList = this.state.permessionMenuList;
		_.each(permessionMenuList, (menu) => {
			if (menu.HasChild && menu.HasChild == true) {
				permessionMenu.push(
					<UISubMenu value={menu.Value} icon={menu.Icon ? menu.Icon : _self.state.defaultIcon} active="active">
						{this.renderPermessionMenuItem(menu.Children)}
					</UISubMenu>
				);
			} else {
				permessionMenu.push(<UIMenuItem value={menu.Value} icon={menu.Icon ? menu.Icon : _self.state.defaultIcon} active="active" onClick={this.goToComponentPage.bind(this, menu.EntranceUrl, menu.Code)} />);
			}
		});
		return permessionMenu;
	}

	renderPermessionMenuItem(menuItems) {

		let menuItemList = [];
		_.each(menuItems, (menuItem) => {
			menuItemList.push(
				<UIMenuItem value={menuItem.Value} icon={menuItem.Icon} active="active" onClick={this.goToComponentPage.bind(this, menuItem.EntranceUrl, menuItem.Code)} />
			);
		})

		return menuItemList;
	}

	goToComponentPage(path, permessionCode) {
		if (path && path.indexOf("$") > -1) {
			let pathArray = path.split("?");
			let key = pathArray[0].substr(1);
			let value = SessionContext.get("project_config")[key];
			window.open(value);
		} else {
			SessionContext.put("PERMISSION-CODE", { "CODE": permessionCode });
			SessionContext.remove("QUICK_NAVIGATION");
			if (!path || path.indexOf("#") < 0) {
				window.location.hash = path;
			} else {
				let actualHost = SessionContext.get("project_config").UI_API_GATEWAY_PROXY.split("//");
				if (actualHost.length > 1) {
					actualHost = actualHost[1];
				} else {
					actualHost = actualHost[0];
				}
				if (actualHost.endsWith("/")) {
					actualHost = actualHost.substring(0, actualHost.length - 1)
				}
				if (!path.startsWith("/")) {
					path = "/" + path;
				}
				let actualHref = location.protocol + "//" + actualHost + path;
				window.open(actualHref);
			}
		}

	}

	selectLanguage(languageId) {
		StoreContext.clear();
		let set_user_lang_url = UrlUtil.getConfigUrl("UI_API_GATEWAY_PROXY", "USER", "SET_USER_LANG");
		AjaxUtil.call(set_user_lang_url, { "langId": languageId }, this.state.postSettings).then(
			(data) => {
				I18nUtil.setSystemI18N(languageId);
				location.reload();
			}
		);
	}

	selectTenant(TenantCode) {
		this.setState({ defaultTenant: TenantCode });
		sessionStorage.setItem("x-ebao-tenant-code", TenantCode);
		location.reload();
	}

	onClickLogout() {
		const config = JSON.parse(sessionStorage.getItem("project_config"));
		logout(config);
	}

}