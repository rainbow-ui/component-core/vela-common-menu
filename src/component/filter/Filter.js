import { UIMessageHelper, UISmartPanelGrid, CodeTable, UIFilter, UISelect } from 'rainbowui-desktop-core';


export default class index extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            searchCondition: '',
            dataSourceContent: [],
            product: null,
            dataSource: [],
            isShowProduct: false,
            isShowProductId: null,
            isShowProductTypeId: null,
            isShowProductTypeName: null,
            productData: []
        };
        this.defaultCondition = [];
        this.searchConditionData = [];
        this.productCode = 2;
        this.productVersion = 3;
        this.codeTableDataSouce = [];
        this.searchParamSource=null,
        this.productConditionId=''
    }
    componentDidMount() {
        this.dataSourceAll();
    }
    componentWillReceiveProps() {
        this.dataSourceAll();
    }
    render() {
        return (
            <div>
                {this.showProduct()}
                <UIFilter id={this.props.id} dataSource={this.state.dataSourceContent}
                    onClick={this.onClickFilterItem.bind(this)}
                    searchCondition={this.state.searchCondition}
                    expandRow='3' />
            </div>
        )
    }
    dataSourceAll() {
        let _self = this;
        let dataSource = _self.props.dataSource;
        let isGroupMultiSelect = _self.props.isGroupMultiSelect;
        let isShowProduct = null;
        let isShowProductId = '';
        let isShowProductTypeId = '';
        let isShowMultiSelect = null;
        let isShowProductTypeName = '';
        let productData = [];
        if (!isGroupMultiSelect) {
            isShowMultiSelect = false;
        } else {
            isShowMultiSelect = _self.props.isShowMultiSelect;
        }
        let content = _self.props.dataSource;
        let dataSourceContent = [];
        _.each(content, (items) => {
            let moduleId = items.Id;
            let childContent = items.Child;
            _.each(childContent, (litemList) => {
                if (_self.productCode != litemList.Id && _self.productVersion != litemList.Id) {
                    let dataContent = {};
                    dataContent.ConditionType = moduleId;
                    dataContent.defaults = _self.defaultCondition;
                    dataContent.id = "" + litemList.Id;
                    dataContent.ConditionId = dataContent.id;
                    dataContent.title = litemList.Name;
                    dataContent.isMultiple = isShowMultiSelect;
                    dataContent.isMultipleShow = _self.props.isMultipleShow;         
                    dataContent.isExpandShow = _self.props.isExpandShow;                               
                    let dataChild = [];
                    _.each(litemList.Child, (childList) => {
                        let childListdata = {};
                        childListdata.value = childList.Name;
                        childListdata.text = childList.Name;
                        childListdata.id = childList.Id;
                        dataChild.push(childListdata);
                    })
                    dataContent.data = dataChild;
                    dataSourceContent.push(dataContent);
                } else if (_self.productCode == litemList.Id || _self.productVersion == litemList.Id) {
                    isShowProduct = true;
                    isShowProductId = litemList.Id;
                    isShowProductTypeId = items.Id;
                    isShowProductTypeName = litemList.Name;
                    productData = litemList.Child;
                }
            })

        });
        if (dataSourceContent) {
            _self.searchConditionData = dataSourceContent;
        }

        // return dataSourceContent;
        _self.state.dataSourceContent = dataSourceContent;
        _self.setState({
            dataSourceContent: _self.state.dataSourceContent,
            isShowProduct: isShowProduct,
            isShowProductId: isShowProductId,
            isShowProductTypeId: isShowProductTypeId,
            isShowProductTypeName: isShowProductTypeName,
            productData: productData
        })
    }
    showProduct() {
        let isShowProduct = this.state.isShowProduct;
        let isShowProductId = this.state.isShowProductId;
        let isShowProductTypeId = this.state.isShowProductTypeId;
        let isShowProductTypeName = this.state.isShowProductTypeName;

        if (isShowProduct) {
            return (
                <UISmartPanelGrid column='3'>
                    <UISelect label={this.state.isShowProductTypeName} widthAllocation="4,8" layout='horizontal' noI18n="true" codeTable={this.codeTableData()} model={this.state} property="product" onChange={this.onChangeProduct.bind(this)}/>
                </UISmartPanelGrid >
            )
        }


    }
    codeTableData() {
        
        let productData = this.state.productData;
        let productConditionData = [];
        _.each(productData,(productDataItems)=>{
            productConditionData.push({id:productDataItems.Id ,text:productDataItems.Name});
        })
        return new CodeTable(productConditionData);
    }
    onChangeProduct(){
        let isShowProduct = this.state.isShowProduct;
        let searchParamFilter = [];
        let productConditionId = '';
        if(isShowProduct){
            let  isShowProductId= this.state.isShowProductId;
            let  isShowProductTypeId = this.state.isShowProductTypeId;
            let  product = this.state.product;
            productConditionId = isShowProductTypeId + '_' + isShowProductId + '_' + product + "";
            if(product){
                searchParamFilter.push(productConditionId);
            }
        }
        this.productConditionId =productConditionId;
        let searchParamFilterOther  = this.searchParamFilter;
        if (searchParamFilterOther){
            searchParamFilterOther.push(searchParamFilter)
        }
        this.props.onSearchHandler(searchParamFilter);
    }
    onClickFilterItem(event) {
        console.log("searchProductFilter",this.productConditionId)
        let isGroupMultiSelect = this.props.isGroupMultiSelect;
        let ruleTypeAjax = true;
        let ruleTypeAjaxName = '';
        let searchParam = {};
        let searchCondition = [];
        let ruleSearchCondition = [];
        let searchConditionType = [];
        let dataJson = event.paramJson;
        let checkData = [];
        _.each(dataJson, (itemList) => {
            if (itemList.ValueList.length != 0) {
                checkData.push({ "conditionId": itemList.id, "conditionName": itemList.ValueList })
            }
        })
        _.each(this.searchConditionData, (dataSource) => {
            let conditionId = dataSource.ConditionId;
            let searchConditionId = {};
            _.each(checkData, (list) => {
                if (conditionId == list.conditionId) {
                    searchConditionId.ConditionType = dataSource.ConditionType;
                    searchConditionId.ConditionId = list.conditionId;
                    if (list.conditionName.length != 0) {
                        _.each(list.conditionName, (dataName) => {
                            _.each(dataSource.data, (current) => {
                                if (dataName == current.text) {
                                    searchConditionId.CurrentId = current.id;
                                    if (!isGroupMultiSelect && searchConditionType.length != 0) {
                                        _.each(searchConditionType, (groupsIdList) => {
                                            if (groupsIdList == searchConditionId.ConditionType) {
                                                // ruleTypeAjaxName = current.text;
                                                ruleSearchCondition = []
                                                ruleTypeAjax = false;
                                                UIMessageHelper.error("只能选择一个模块，请重新选择");
                                            } else {
                                                searchConditionType.push(searchConditionId.ConditionType);
                                                searchCondition.push(searchConditionId.ConditionType + '_' + searchConditionId.ConditionId + '_' + searchConditionId.CurrentId);
                                                ruleSearchCondition.push(current.text)
                                                searchParam.searchCondition = searchCondition;
                                                searchParam.searchName = ruleSearchCondition;
                                            }
                                        })
                                    } else {
                                        searchConditionType.push(searchConditionId.ConditionType);
                                        searchCondition.push(searchConditionId.ConditionType + '_' + searchConditionId.ConditionId + '_' + searchConditionId.CurrentId);
                                        ruleSearchCondition.push(current.text)
                                        searchParam.searchCondition = searchCondition;
                                        searchParam.searchName = ruleSearchCondition;
                                    }

                                }
                            })
                        })
                    }

                }
            })
        })
        let isShowProduct = this.state.isShowProduct;
        let searchParamFilter =[];
        if(searchParam.searchCondition){
            searchParamFilter = searchParam.searchCondition;
        }
        let  product = this.state.product;
        if(this.productConditionId && product){
            searchParamFilter.push(this.productConditionId)
        }
     
        if (ruleTypeAjax) {
            this.defaultCondition = searchParam.searchName;
            this.searchParamFilter = searchParamFilter;
            this.props.onSearchHandler(searchParamFilter)
        } else {
            this.setState({})
        }
    
    }
 

}